//
//  AppDelegate.swift
//  JackCoPilot
//
//  Created by Éric Bavu on 01/12/2020.
//

import Cocoa
import AMCoreAudio

@main
class AppDelegate: NSObject, NSApplicationDelegate, NSUserNotificationCenterDelegate {

        
    @IBOutlet weak var menu: NSMenu?
    @IBOutlet weak var startJackfrommenu: NSMenuItem?
    @IBOutlet weak var stopJackfrommenu: NSMenuItem?
    @IBOutlet weak var showJackInfo: NSMenuItem?

    
    @IBAction func showPreferences(_ sender: Any) {
   //         let storyboard = NSStoryboard(name: "Main", bundle: nil)
   //         guard let vc = storyboard.instantiateController(withIdentifier: .init(stringLiteral: "mainView")) as? ViewController else { return }
   //     let window = NSWindow(contentViewController: vc)
   //     window.makeKeyAndOrderFront(nil)
        NSApplication.shared.windows.last!.makeKeyAndOrderFront(nil)
        NSApplication.shared.activate(ignoringOtherApps: true)
        NSApp.setActivationPolicy(.regular)
    }
    
    
    
    @IBAction func startJackfrommenuclicked(_ sender: Any) {
        startJackServerfromMenu()

    }
    
    
    
    @IBAction func stopJackfrommenuclicked(_ sender: Any) {
        stopJackServerfromMenu()
    }
    
    
    var statusItem: NSStatusItem?
    
    var dateTimeView: DateTimeView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)

        let itemImage = NSImage(named: "menubar_icon")
        itemImage?.isTemplate = true
        statusItem?.button?.image = itemImage
        if let menu = menu {
            statusItem?.menu = menu
            menu.delegate = self
        }
        statusItem?.button?.appearsDisabled = true
        
        if let item = showJackInfo {
            dateTimeView = DateTimeView(frame: NSRect(x: 0.0, y: 0.0, width: 280.0, height: 210.0))
            item.view = dateTimeView
            }
        
    }
    
    
    fileprivate func stopJackServerfromMenu() {
        
        
        let kill_jack_if_running = ["-c", "if pgrep jackdmp 2>/dev/null; then pkill jackdmp;  fi ;"]
        
        let killjackprocess = Process()
        
        killjackprocess.launchPath = "/bin/bash"
            
        let killjack_arguments:[String] = kill_jack_if_running
            
        killjackprocess.arguments = killjack_arguments
        
        let pipe=Pipe()
        killjackprocess.standardOutput = pipe
        
        killjackprocess.launch()
                
        
        killjackprocess.waitUntilExit()
        
        UserDefaults.standard.set(false , forKey: "jack_server_launched_by_JackCoPilot")
        UserDefaults.standard.set(false , forKey: "jack_server_is_active")
    }
    
    
    
    fileprivate func startJackServerfromMenu(){
                
        let check_if_jack_is_running = ["-c", "pgrep jackdmp 2>/dev/null"]
        
        let checkjackprocess = Process()
        
        checkjackprocess.launchPath = "/bin/bash"
            
        let checkjack_arguments:[String] = check_if_jack_is_running
            
        checkjackprocess.arguments = checkjack_arguments
        
        let pipe=Pipe()
        checkjackprocess.standardOutput = pipe
        
        checkjackprocess.launch()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()

        if let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)  {
            if output != "" {
                JackStartNotification_already_launched()
            }
            else {
 
            let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
            dispatchQueue.async{
            let jackprocess = Process()
            
            jackprocess.launchPath = "/bin/bash"
        
            jackprocess.arguments = UserDefaults.standard.stringArray(forKey: "jack_command_to_launch")
                
            UserDefaults.standard.set(true , forKey: "jack_server_launched_by_JackCoPilot")
            UserDefaults.standard.set(true , forKey: "jack_server_is_active")

                
            jackprocess.launch()
            jackprocess.waitUntilExit()
            
        }
            
        }
            }
        
        checkjackprocess.waitUntilExit()
        
        Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false) { timer in
            let check_if_jack_is_running2 = ["-c", "pgrep jackdmp 2>/dev/null"]
        
        let checkjackprocess2 = Process()
        
        checkjackprocess2.launchPath = "/bin/bash"
            
        let checkjack_arguments2:[String] = check_if_jack_is_running2
            
        checkjackprocess2.arguments = checkjack_arguments2
        
        let pipe2=Pipe()
        checkjackprocess2.standardOutput = pipe2
        
        checkjackprocess2.launch()
        
        let data2 = pipe2.fileHandleForReading.readDataToEndOfFile()

        if let output2 = NSString(data: data2, encoding: String.Encoding.utf8.rawValue)  {
            if output2 != "" {
            }
            else {
                
                UserDefaults.standard.set(false , forKey: "jack_server_launched_by_JackCoPilot")
                UserDefaults.standard.set(false , forKey: "jack_server_is_active")

                self.JackStartNotification_wrong()            }
            }
        checkjackprocess2.waitUntilExit()
        }
        

    }
    
    
  func JackStartNotification() -> Void {
                let notification = NSUserNotification()
                notification.title = "The Jack server is running."
    if UserDefaults.standard.bool(forKey: "jack_server_launched_by_JackCoPilot") == true {
        notification.subtitle = "Launched by Jack Copilot "
    notification.informativeText = "Click the menubar icon to view the used parameters."
}
    else {
        notification.subtitle = "Launched by external software."
        notification.informativeText = "The parameters passed to Jack are unknown."

    }
    NSUserNotificationCenter.default.delegate = self
        notification.deliveryDate = Date(timeIntervalSinceNow: 0.5)
        NSUserNotificationCenter.default.scheduleNotification(notification)
    }
    
    
fileprivate  func JackStartNotification_wrong() -> Void {
                  let notification = NSUserNotification()
                  notification.title = "Something went wrong. The Jack Server dit not start."
                  NSUserNotificationCenter.default.delegate = self
          notification.deliveryDate = Date(timeIntervalSinceNow: 0.5)
          NSUserNotificationCenter.default.scheduleNotification(notification)
      }
    
fileprivate  func JackStartNotification_already_launched() -> Void {
                      let notification = NSUserNotification()
                      notification.title = "The Jack Server was already running !"
                      notification.subtitle = "Sub Test."
                      NSUserNotificationCenter.default.delegate = self
              notification.deliveryDate = Date(timeIntervalSinceNow: 0.5)
              NSUserNotificationCenter.default.scheduleNotification(notification)
          }
    
    
func JackStopNotification() -> Void {
                      let notification = NSUserNotification()
                      notification.title = "The Jack Server has been stopped."
            notification.setValue(true, forKey: "_showsButtons")
            notification.hasActionButton = true
    notification.actionButtonTitle = "Show preferences"
    
                      NSUserNotificationCenter.default.delegate = self
              notification.deliveryDate = Date(timeIntervalSinceNow: 0.5)
              NSUserNotificationCenter.default.scheduleNotification(notification)
          }

 func userNotificationCenter(_ center: NSUserNotificationCenter,
                                             shouldPresent notification: NSUserNotification) -> Bool {
            return true
    }
    
    func userNotificationCenter(_ center: NSUserNotificationCenter, didActivate notification: NSUserNotification) {
        switch (notification.activationType) {
        case .actionButtonClicked:
            NSApplication.shared.windows.last!.makeKeyAndOrderFront(nil)
            NSApplication.shared.activate(ignoringOtherApps: true)
            NSApp.setActivationPolicy(.regular)            default:
                break;
        }
    }

    

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        //NSApplication.shared.windows.last!.close()
        //NSApp.setActivationPolicy(.accessory)
    }


    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}





extension AppDelegate: NSMenuDelegate {
 
    func menuWillOpen(_ menu: NSMenu) {
        
        dateTimeView?.startTimer()

    }
    
    func menuDidClose(_ menu: NSMenu) {
        dateTimeView?.stopTimer()
    
}

}

