//
//  ViewController.swift
//  JackCoPilot
//
//  Created by Éric Bavu on 01/12/2020.
//

import Cocoa
import AMCoreAudio
import Foundation
import AppKit



class ViewController: NSViewController, NSWindowDelegate {

    
    @IBOutlet weak var deviceListPopUpButton: NSPopUpButton!
    
    @IBOutlet weak var outputdeviceListPopUpButton: NSPopUpButton!
    
    @IBOutlet weak var currentInputSampleRate: NSTextField!
        
    @IBOutlet weak var currentOutputSampleRate: NSTextField!
        
    @IBOutlet weak var NominalSampleRatesPopupButton: NSPopUpButton!
    
    @IBOutlet weak var BufferPopupButton: NSPopUpButton!
    
    @IBOutlet weak var theoreticalLatency: NSTextField!
    
    @IBOutlet weak var hogModeCheckbox: NSButton!
    
    @IBOutlet weak var clockDriftCheckBox: NSButton!
    
    @IBOutlet weak var midiCheckbox: NSButton!
    
    @IBOutlet weak var jackStartButton: NSButton!
    
    @IBOutlet weak var jackStopButton: NSButton!
    
    @IBOutlet weak var errorMessagesbox: NSTextField!
    
    
    @IBOutlet weak var openmidiconfig: NSButton!
    
    
    @IBOutlet weak var opensoundpref: NSButton!
    
    
    @IBOutlet weak var openpatchage: NSButton!
    
    fileprivate let unknownValue = "<Unknown>"
    fileprivate let unsupportedValue = "<Unsupported>"
    
    var listofAgregateDevices = [Int]()
    var listofAgregateInputDevices = [Int]()
    var listofAgregateOutputDevices = [Int]()
    
    var timer = Timer()

    struct chosenPreferences {

      // 1
      var chosenInputDevice: String {
        get {
          // 2
            let savedInputDevice = UserDefaults.standard.string(forKey: "chosenInputDevice")
          if savedInputDevice != nil {
            return savedInputDevice!
          }
          // 3
          return ""
        }
        set {
          // 4
          UserDefaults.standard.set(newValue, forKey: "chosenInputDevice")
        }
      }
        
        // 1
        var chosenOutputDevice: String {
          get {
            // 2
              let savedOutputDevice = UserDefaults.standard.string(forKey: "chosenOutputDevice")
            if savedOutputDevice != nil {
              return savedOutputDevice!
            }
            // 3
            return ""
          }
          set {
            // 4
            UserDefaults.standard.set(newValue, forKey: "chosenOutputDevice")
          }
        }
        
        var chosenAggregateDevice: String {
          get {
            // 2
              let savedAggregateDevice = UserDefaults.standard.string(forKey: "chosenAggregateDevice")
            if savedAggregateDevice != nil {
              return savedAggregateDevice!
            }
            // 3
            return ""
          }
          set {
            // 4
            UserDefaults.standard.set(newValue, forKey: "chosenAggregateDevice")
          }
        }
        
        var chosenAggregateDeviceTag: Int {
          get {
            // 2
              let savedAggregateDeviceTag = UserDefaults.standard.integer(forKey: "chosenAggregateDeviceTag")
            if savedAggregateDeviceTag > 0 {
                return savedAggregateDeviceTag
            }
            // 3
            return -1
          }
          set {
            // 4
            UserDefaults.standard.set(newValue, forKey: "chosenAggregateDeviceTag")
          }
        }
        
        
        
        // 1
        var chosenSampleRate: String {
          get {
            // 2
              let savedSampleRate = UserDefaults.standard.string(forKey: "chosenSampleRate")
            if savedSampleRate != nil {
              return savedSampleRate!
            }
            // 3
            return ""
          }
          set {
            // 4
            UserDefaults.standard.set(newValue, forKey: "chosenSampleRate")
          }
        }
        
        var chosenBufferSize: String {
          get {
            // 2
              let savedBufferSize = UserDefaults.standard.string(forKey: "chosenBufferSize")
            if savedBufferSize != nil {
              return savedBufferSize ?? "256"
            }
            // 3
            return "256"
          }
          set {
            // 4
            UserDefaults.standard.set(newValue, forKey: "chosenBufferSize")
          }
        }
        
        
        var chosenHogMode: String {
          get {
            // 2
              let savedHogMode = UserDefaults.standard.string(forKey: "chosenHogMode")
            if savedHogMode != nil {
              return savedHogMode!
            }
            // 3
            return ""
          }
          set {
            // 4
            UserDefaults.standard.set(newValue, forKey: "chosenHogMode")
          }
        }
        
        var chosenClockDrift: String {
          get {
            // 2
              let savedClockDrift = UserDefaults.standard.string(forKey: "chosenClockDrift")
            if savedClockDrift != nil {
              return savedClockDrift!
            }
            // 3
            return ""
          }
          set {
            // 4
            UserDefaults.standard.set(newValue, forKey: "chosenClockDrift")
          }
        }
        
        var chosenMidiUsage: String {
          get {
            // 2
              let savedMidiUsage = UserDefaults.standard.string(forKey: "chosenMidiUsage")
            if savedMidiUsage != nil {
              return savedMidiUsage!
            }
            // 3
            return ""
          }
          set {
            // 4
            UserDefaults.standard.set(newValue, forKey: "chosenMidiUsage")
          }
        }
        
    }
    
    var chosenprefs = chosenPreferences()
    
    
    
    let appDelegate = NSApp.delegate as! AppDelegate
    
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
            if let audioDevice = representedObject as? AudioDevice {
                    populateDeviceInformation(device: audioDevice)
                    //getAgregateDeviceList()
                    
                //populatePlaybackStreamPopUpButton(device: audioDevice)
                //populateRecordingStreamPopUpButton(device: audioDevice)

                // Propagate representedObject changes in child controllers
                //inputViewController?.representedObject = audioDevice
                //outputViewController?.representedObject = audioDevice
            }
        }
    }
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
                  
        // Do any additional setup after loading the view.
        // Subscribe to events
        
        
        errorMessagesbox.stringValue = "Jack Copilot has started. Configure the jack server parameters."
        
        NotificationCenter.defaultCenter.subscribe(self, eventType: AudioHardwareEvent.self, dispatchQueue: DispatchQueue.main)
        NotificationCenter.defaultCenter.subscribe(self, eventType: AudioDeviceEvent.self, dispatchQueue: DispatchQueue.main)
        NotificationCenter.defaultCenter.subscribe(self, eventType: AudioStreamEvent.self, dispatchQueue: DispatchQueue.main)

        jackStopButton.isHidden = true
        appDelegate.stopJackfrommenu?.isHidden = true


        
        // Populate device list
        populateDeviceList()
        populateOutputDeviceList()
        getAgregateDeviceList()
        populateBufferSizes()
        
        // Set view controller's represented object
        if let selectedItem = deviceListPopUpButton.selectedItem {
            let deviceID = AudioObjectID(selectedItem.tag)
            representedObject = AudioDevice.lookup(by: deviceID)
        }
        
        if let selectedouputItem = outputdeviceListPopUpButton.selectedItem {
            let deviceID = AudioObjectID(selectedouputItem.tag)
            representedObject = AudioDevice.lookup(by: deviceID)
        }
        
        var Jackcurrentstate = 0{
            didSet {
                if oldValue != Jackcurrentstate {
                if Jackcurrentstate == 1 {
                    jackStopButton.isHidden = false
                    jackStopButton.isEnabled = true
                    jackStartButton.isHidden = true
                    deviceListPopUpButton.isEnabled = false
                    outputdeviceListPopUpButton.isEnabled = false
                    NominalSampleRatesPopupButton.isEnabled = false
                    BufferPopupButton.isEnabled = false
                    hogModeCheckbox.isEnabled = false
                    clockDriftCheckBox.isEnabled = false
                    midiCheckbox.isEnabled = false
                    appDelegate.statusItem?.button?.appearsDisabled = false
                    appDelegate.stopJackfrommenu?.isHidden = false
                    appDelegate.startJackfrommenu?.isHidden = true
                    errorMessagesbox.stringValue =  "⚡⚡ The Jack Server is currently running. "

                }
                else if Jackcurrentstate == 0 {
                    jackStartButton.isHidden = false
                    jackStartButton.isEnabled = true
                    jackStopButton.isHidden = true
                    deviceListPopUpButton.isEnabled = true
                    outputdeviceListPopUpButton.isEnabled = true
                    NominalSampleRatesPopupButton.isEnabled = true
                    BufferPopupButton.isEnabled = true
                    hogModeCheckbox.isEnabled = true
                    clockDriftCheckBox.isEnabled = true
                    midiCheckbox.isEnabled = true
                    appDelegate.statusItem?.button?.appearsDisabled = true
                    appDelegate.stopJackfrommenu?.isHidden = true
                    appDelegate.startJackfrommenu?.isHidden = false
                    errorMessagesbox.stringValue =  "💨 The Jack Server has stopped"


                }
            }
            }
        }
        
        checkifJackExists()

        var isactiveJackState = false
        var wholaunchedJackState = false

        
        if #available(OSX 10.12, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 4.0, repeats: true) { timer in
                Jackcurrentstate = self.checkJackRunning()
                (wholaunchedJackState , isactiveJackState) = self.showJackNotification(launchedby_oldStateValue: wholaunchedJackState , isactive_oldStateValue: isactiveJackState)
            }
            timer.fire()
        } else {
            // Fallback on earlier versions
        }
        
        showExistingPrefs()
        saveJackcommand()

        
        
    }
    
    

    deinit {
        NotificationCenter.defaultCenter.unsubscribe(self, eventType: AudioHardwareEvent.self)
        NotificationCenter.defaultCenter.unsubscribe(self, eventType: AudioDeviceEvent.self)
        NotificationCenter.defaultCenter.unsubscribe(self, eventType: AudioStreamEvent.self)
    }
    
    
    override func viewWillAppear() {
        // we choose to delegate ourselves. must be done here, in viewDidLoad window is nil
        let window = self.view.window
        window!.delegate = self
    }
    

    
    
    
    func windowShouldClose(_ sender: NSWindow) -> Bool {
        let userDefaultsKey = "menubarinfomessage"
        if UserDefaults.standard.bool(forKey: userDefaultsKey) == false {
            let alert = NSAlert()
            alert.messageText = "Closing Jack CoPilot window"
            alert.informativeText = "Jack Copilot will stay alive in the menubar, as a background process.\n\nAt any moment, you can easilly reach the main window again by clicking on the Jack Copilot icon located in the menubar, in the top right hand side of you screen. \n\nIf you want to quit totally Jack Copilot, click on Quit.\n\nPlease note that if the Jack server has already been launched, it will remain active after quitting Jack CoPilot."
            alert.alertStyle = .warning
            alert.showsSuppressionButton = true
            alert.suppressionButton?.title = "I got it, don't show me this message again."
            alert.addButton(withTitle: "Close window")
            alert.addButton(withTitle: "Quit")
            
        alert.beginSheetModal(for: self.view.window!) { (response) in
                   
            if let supress = alert.suppressionButton {
                let state = supress.state
                switch state {
                case NSControl.StateValue.on:
                    UserDefaults.standard.set(true, forKey: userDefaultsKey)
                default: break
                }
            }
            
            if response == .alertFirstButtonReturn {
                NSApplication.shared.hide(nil)
                NSApp.setActivationPolicy(.accessory)
                
            } else {
                NSApp.terminate(nil)
                
            }
    }
    }

        return false
        }
    
    


        func applicationDidBecomeActive(_ notification: Notification) {
            NSApplication.shared.unhide(nil)
            NSApp.setActivationPolicy(.regular)
            saveJackcommand()

        }
    
    @IBAction func showDevice(_ sender: AnyObject) {
        
        if let popUpButton = sender as? NSPopUpButton, let item = popUpButton.selectedItem {
                if item.tag > 0 {
                    let deviceID = AudioObjectID(item.tag)
                    representedObject = AudioDevice.lookup(by: deviceID)
                        if let device = representedObject as? AudioDevice {
                            if device.transportType?.rawValue == "aggregate" {
                                hogModeCheckbox.state = .off
                                outputdeviceListPopUpButton.isEnabled = false
                                if listofAgregateOutputDevices.contains(Int(item.tag)){
                                outputdeviceListPopUpButton.selectItem(withTag : item.tag)
                                    errorMessagesbox.stringValue =  "🔔 Automatically selecting the same macOS aggregate device for input and output."
                                    chosenprefs.chosenAggregateDevice = deviceListPopUpButton.selectedItem!.title
                                    chosenprefs.chosenAggregateDeviceTag = deviceListPopUpButton.selectedItem!.tag
                                    //chosenprefs.chosenOutputDevice = outputdeviceListPopUpButton.selectedItem!.title
                            }
                                else{
                                    outputdeviceListPopUpButton.selectItem(withTag : -2)
                                    errorMessagesbox.stringValue =  "⚠️ This aggregate device has currently only inputs !"
                                    chosenprefs.chosenAggregateDevice = deviceListPopUpButton.selectedItem!.title
                                    chosenprefs.chosenAggregateDeviceTag = deviceListPopUpButton.selectedItem!.tag
                                    //chosenprefs.chosenOutputDevice = outputdeviceListPopUpButton.selectedItem!.title
                                }
                                
                            }
                            else {
                                errorMessagesbox.stringValue = ""
                                outputdeviceListPopUpButton.isEnabled = true
                                for id in listofAgregateDevices{
                                if let menuitem = outputdeviceListPopUpButton.item(withTag: id) {
                                    menuitem.isEnabled = false
                                    chosenprefs.chosenAggregateDevice = ""
                                    chosenprefs.chosenAggregateDeviceTag = -1
                                   

                                }
                            }
                }
                        }
        }
                else{
                    errorMessagesbox.stringValue = ""
                }
        
        populateDeviceInformation(device: representedObject as! AudioDevice)
}
        chosenprefs.chosenInputDevice = deviceListPopUpButton.selectedItem!.title

        saveJackcommand()


    }


    
    
    @IBAction func updateBufferSize(_ sender: AnyObject) {
       // if let popUpButton = sender as? NSPopUpButton, let item = popUpButton.selectedItem {
      //      if let currentbuffersize = item.representedObject as? Float64 {
      //      theoreticalLatency.stringValue = String(currentbuffersize)
      //          print(currentbuffersize)
      //  }
            populateDeviceInformation(device: representedObject as! AudioDevice)
       chosenprefs.chosenBufferSize = BufferPopupButton.selectedItem!.title

        saveJackcommand()
   // }
    }
    
    
    
    @IBAction func showOutputDevice(_ sender: AnyObject) {
        if let popUpButton = sender as? NSPopUpButton, let item = popUpButton.selectedItem {
            if item.tag > 0 {
            let deviceID = AudioObjectID(item.tag)
                representedObject = AudioDevice.lookup(by: deviceID)
                    if let device = representedObject as? AudioDevice {
                        if device.transportType?.rawValue == "aggregate" {
                            hogModeCheckbox.state = .off
                            deviceListPopUpButton.isEnabled = false
                            if listofAgregateInputDevices.contains(Int(item.tag)){
                            deviceListPopUpButton.selectItem(withTag : item.tag)
                                errorMessagesbox.stringValue =  "🔔 Automatically selecting the same macOS aggregate device for input and output."
                                chosenprefs.chosenAggregateDevice = outputdeviceListPopUpButton.selectedItem!.title
                                chosenprefs.chosenAggregateDeviceTag = outputdeviceListPopUpButton.selectedItem!.tag
                                //chosenprefs.chosenInputDevice = deviceListPopUpButton.selectedItem!.title


                        }
                            else{
                                deviceListPopUpButton.selectItem(withTag : -1)
                                errorMessagesbox.stringValue =  "⚠️ This aggregate device has currently only outputs !"
                                chosenprefs.chosenAggregateDevice = outputdeviceListPopUpButton.selectedItem!.title
                                chosenprefs.chosenAggregateDeviceTag = outputdeviceListPopUpButton.selectedItem!.tag
                                //chosenprefs.chosenInputDevice = deviceListPopUpButton.selectedItem!.title

                            }
                            
                        }
                        else {
                            chosenprefs.chosenAggregateDevice = ""
                            chosenprefs.chosenAggregateDeviceTag = -1
                            errorMessagesbox.stringValue = ""
                            deviceListPopUpButton.isEnabled = true
                            for id in listofAgregateDevices{
                            if let menuitem = deviceListPopUpButton.item(withTag: id) {
                                menuitem.isEnabled = false
                                //deviceListPopUpButton.selectItem(at : 0)

                            }
                        }
            }
                        }
            }
            else{
                errorMessagesbox.stringValue = ""
            }
        populateDeviceInformation(device: representedObject as! AudioDevice)
        }
        
        chosenprefs.chosenOutputDevice = outputdeviceListPopUpButton.selectedItem!.title

        saveJackcommand()


    }


    
    
    @IBAction func updateSampleRate(_ sender: Any) {
                if let popUpButton = sender as? NSPopUpButton, let item = popUpButton.selectedItem {
                if let sampleRate = item.representedObject as? Float64 {
                    if (deviceListPopUpButton.selectedItem!.tag > 0) {
                    let input_device_id = AudioObjectID( deviceListPopUpButton.selectedItem!.tag )
                    let input_device = AudioDevice.lookup(by: input_device_id)
                    if input_device?.setNominalSampleRate(sampleRate) == false {
                        errorMessagesbox.stringValue =  "☢️ Unable to set nominal sample rate for Input."
                            }
                    }

                    if (outputdeviceListPopUpButton.selectedItem!.tag > 0) {
                    let output_device_id = AudioObjectID( outputdeviceListPopUpButton.selectedItem!.tag )
                    let output_device = AudioDevice.lookup(by: output_device_id)
                    if output_device?.setNominalSampleRate(sampleRate) == false {
                        errorMessagesbox.stringValue =  "☢️ Unable to set nominal sample rate for Output."
                            }
                    }
                    }
                    populateDeviceInformation(device: representedObject as! AudioDevice)

                }
        if NominalSampleRatesPopupButton.selectedItem!.title != unsupportedValue {
        chosenprefs.chosenSampleRate = NominalSampleRatesPopupButton.selectedItem!.title
        }
        
        saveJackcommand()

            }
        
    
    
    @IBAction func updateHogMode(_ sender: Any) {
        if hogModeCheckbox.state == .on{
            chosenprefs.chosenHogMode = "On"
        }
        else {
            chosenprefs.chosenHogMode = "Off"
        }
        saveJackcommand()
    }
    
    
    @IBAction func updateClockDrift(_ sender: Any) {
        if clockDriftCheckBox.state == .on{
            chosenprefs.chosenClockDrift = "On"
        }
        else {
            chosenprefs.chosenClockDrift = "Off"
        }
        saveJackcommand()
    }
    
    
    @IBAction func updateMidiMode(_ sender: Any) {
        if midiCheckbox.state == .on{
            chosenprefs.chosenMidiUsage = "On"
        }
        else {
            chosenprefs.chosenMidiUsage = "Off"
        }
        
        saveJackcommand()


    }
    
    
    @IBAction func StartJack(_ sender: Any) {
        
        jackStartButton.isEnabled = false
        
        saveJackcommand()

            startJackServer()
            
            }

    @IBAction func StopJack(_ sender:Any) {
        jackStopButton.isEnabled = false
        stopJackServer()
    }
    
    
    
    @IBAction func launchmidiconfig(_ sender: Any) {
        
        guard let url = NSWorkspace.shared.urlForApplication(withBundleIdentifier: "com.apple.audio.AudioMIDISetup") else { return }
        NSWorkspace.shared.open(url)
        
        saveJackcommand()

    }
    
    
    @IBAction func launchsoundprefs(_ sender: Any) {
    
    let url = "/System/Library/PreferencePanes/Sound.prefPane"
        NSWorkspace.shared.open(URL(fileURLWithPath: url))
        
        saveJackcommand()

    }

    
    @IBAction func launchpatchage(_ sender: Any) {
        guard let url = NSWorkspace.shared.urlForApplication(withBundleIdentifier: "net.drobilla.Patchage") else { return }
        NSWorkspace.shared.open(url)
        
        saveJackcommand()

    }
    
    
// MARK: - Private
    
    
    fileprivate func showExistingPrefs() {
        let chosenInputDevicetoapply = chosenprefs.chosenInputDevice
        if chosenInputDevicetoapply != "" {
            if deviceListPopUpButton.itemTitles.contains(chosenInputDevicetoapply) {
        deviceListPopUpButton.selectItem(withTitle: chosenInputDevicetoapply)
            }
            else {
            }
        }
        
        let chosenOutputDevicetoapply = chosenprefs.chosenOutputDevice
        if chosenOutputDevicetoapply != "" {
            if outputdeviceListPopUpButton.itemTitles.contains(chosenOutputDevicetoapply) {
            outputdeviceListPopUpButton.selectItem(withTitle: chosenOutputDevicetoapply)
            }
            else {
            }
        
        }
        
        let chosenSampleRatetoapply = chosenprefs.chosenSampleRate
        if chosenSampleRatetoapply != "" {
            if NominalSampleRatesPopupButton.itemTitles.contains(chosenSampleRatetoapply) {
                NominalSampleRatesPopupButton.selectItem(withTitle: chosenSampleRatetoapply)
            }
            else {
            }
        }
        
        let chosenBufferSizetoapply = chosenprefs.chosenBufferSize
        if chosenBufferSizetoapply != "" {
            if BufferPopupButton.itemTitles.contains(chosenBufferSizetoapply) {
                BufferPopupButton.selectItem(withTitle: chosenBufferSizetoapply)
            }
            else {
            }
            
        }
        
        let chosenHogModetoapply = chosenprefs.chosenHogMode
        if chosenHogModetoapply != ""{
        if chosenHogModetoapply == "On"{
            hogModeCheckbox.state = .on
        }
        else
        {hogModeCheckbox.state = .off}
        }
        
            
        let chosenClockDrifttoapply = chosenprefs.chosenClockDrift
        if chosenClockDrifttoapply != ""{

        if chosenClockDrifttoapply == "On"{
        clockDriftCheckBox.state = .on
        }
        else
        {clockDriftCheckBox.state = .off}
        }
        
        
        let chosenMidiUsagetoapply = chosenprefs.chosenMidiUsage
        if chosenMidiUsagetoapply != ""{
        if chosenMidiUsagetoapply == "On"{
        midiCheckbox.state = .on
        }
        else
        {midiCheckbox.state = .off}
        }
        
    }
    
    
    fileprivate func format(sampleRate: Float64) -> String {
        return String(format: "%.1f kHz", sampleRate / 1000)
    }
    
    
    fileprivate func userNotificationCenter(center: NSUserNotificationCenter, shouldPresentNotification notification: NSUserNotification) -> Bool {
        return true
    }
    
    
    fileprivate func saveJackcommand(){
    chosenprefs.chosenInputDevice = deviceListPopUpButton.selectedItem!.title
    chosenprefs.chosenOutputDevice = outputdeviceListPopUpButton.selectedItem!.title
    if NominalSampleRatesPopupButton.selectedItem!.title != unsupportedValue {
    chosenprefs.chosenSampleRate = NominalSampleRatesPopupButton.selectedItem!.title
    }
    chosenprefs.chosenBufferSize = BufferPopupButton.selectedItem!.title
    if hogModeCheckbox.state == .on{
        chosenprefs.chosenHogMode = "On"
    }
    else {
        chosenprefs.chosenHogMode = "Off"
    }
    if clockDriftCheckBox.state == .on{
        chosenprefs.chosenClockDrift = "On"
    }
    else {
        chosenprefs.chosenClockDrift = "Off"
    }
    if midiCheckbox.state == .on{
        chosenprefs.chosenMidiUsage = "On"
    }
    else {
        chosenprefs.chosenMidiUsage = "Off"
    }
    
    
    var jack_command_parameters = ["/usr/local/bin/jackdmp", "-R", "-d", "coreaudio"]

    jack_command_parameters.append("-p ")
    jack_command_parameters.append(BufferPopupButton.selectedItem!.title)
    
    if hogModeCheckbox.state.rawValue == 1 {
    jack_command_parameters.append("-H")
    }
    
    if clockDriftCheckBox.state.rawValue == 1 {
    jack_command_parameters.append("-s")
    }
    
    jack_command_parameters.insert("-r", at: 4)
    jack_command_parameters.insert(String(Int((NominalSampleRatesPopupButton.selectedItem?.representedObject as? Float64)!)), at: 5)
    
    if midiCheckbox.state.rawValue == 1 {
        jack_command_parameters.insert("-X", at: 2)
        jack_command_parameters.insert("coremidi", at: 3)
    }
    
    if chosenprefs.chosenAggregateDeviceTag > 0 {
        jack_command_parameters.append("-d")
        jack_command_parameters.append("\"" + ((AudioDevice.lookup(by: AudioObjectID(chosenprefs.chosenAggregateDeviceTag )))?.uid)! + "\"")
    }
    
    else {
    
    if deviceListPopUpButton.selectedItem!.tag > 0 {
    jack_command_parameters.append("-C")
    jack_command_parameters.append("\"" + ((AudioDevice.lookup(by: AudioObjectID( deviceListPopUpButton.selectedItem!.tag )))?.uid)! + "\"")
    }
    
    if outputdeviceListPopUpButton.selectedItem!.tag > 0 {
    jack_command_parameters.append("-P")
    jack_command_parameters.append("\"" + ((AudioDevice.lookup(by: AudioObjectID( outputdeviceListPopUpButton.selectedItem!.tag )))?.uid)! + "\"")
        }
      
    }
        
    
    var jack_command_arguments:[String] = [jack_command_parameters.joined(separator: " ")]
    jack_command_arguments.insert("-c", at:0)
    
    
    UserDefaults.standard.set(jack_command_arguments , forKey: "jack_command_to_launch")
}
    
    fileprivate func startJackServer(){
                
        let check_if_jack_is_running = ["-c", "pgrep jackdmp 2>/dev/null"]
        
        let checkjackprocess = Process()
        
        checkjackprocess.launchPath = "/bin/bash"
            
        let checkjack_arguments:[String] = check_if_jack_is_running
            
        checkjackprocess.arguments = checkjack_arguments
        
        let pipe=Pipe()
        checkjackprocess.standardOutput = pipe
        
        checkjackprocess.launch()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()

        if let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)  {
            if output != "" {
            errorMessagesbox.stringValue =  "⚠️ ⚠️ The Jack Server was already running ! "
            }
            else {
        
        errorMessagesbox.stringValue =  "💈 The Jack Server is starting ... "
        
        
        if ( (deviceListPopUpButton.selectedItem!.tag < 0) && (outputdeviceListPopUpButton.selectedItem!.tag < 0)) {
        
            errorMessagesbox.stringValue =  "⛔ Please select at least one input or one output ! "
            
        }
        else{
            
            
            let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
            dispatchQueue.async{
            let jackprocess = Process()
            
            jackprocess.launchPath = "/bin/bash"
        
            jackprocess.arguments = UserDefaults.standard.stringArray(forKey: "jack_command_to_launch")

            UserDefaults.standard.set(true , forKey: "jack_server_launched_by_JackCoPilot")
            UserDefaults.standard.set(true , forKey: "jack_server_is_active")

            jackprocess.launch()

            jackprocess.waitUntilExit()
            
        }
            
        }
            }
        }
        
        checkjackprocess.waitUntilExit()
        
        Timer.scheduledTimer(withTimeInterval: 4.0, repeats: false) { timer in
            let check_if_jack_is_running2 = ["-c", "pgrep jackdmp 2>/dev/null"]
        
        let checkjackprocess2 = Process()
        
        checkjackprocess2.launchPath = "/bin/bash"
            
        let checkjack_arguments2:[String] = check_if_jack_is_running2
            
        checkjackprocess2.arguments = checkjack_arguments2
        
        let pipe2=Pipe()
        checkjackprocess2.standardOutput = pipe2
        
        checkjackprocess2.launch()
        
        let data2 = pipe2.fileHandleForReading.readDataToEndOfFile()

        if let output2 = NSString(data: data2, encoding: String.Encoding.utf8.rawValue)  {
            if output2 != "" {
                self.errorMessagesbox.stringValue =  "⚡⚡ The Jack Server is now running. "
                
            }
            else {
                self.errorMessagesbox.stringValue =  "⚠️ Something went wrong. The Jack server did not start."
                UserDefaults.standard.set(false , forKey: "jack_server_launched_by_JackCoPilot")
                UserDefaults.standard.set(false , forKey: "jack_server_is_active")

                self.jackStartButton.isEnabled = true

            }
            }
        checkjackprocess2.waitUntilExit()
        }
        

    }
    
    
    fileprivate func stopJackServer() {
        
        errorMessagesbox.stringValue =  "💈 The Jack Server is stopping ... "

        
        let kill_jack_if_running = ["-c", "if pgrep jackdmp 2>/dev/null; then pkill jackdmp;  fi ;"]
        
        let killjackprocess = Process()
        
        killjackprocess.launchPath = "/bin/bash"
            
        let killjack_arguments:[String] = kill_jack_if_running
            
        killjackprocess.arguments = killjack_arguments
        
        let pipe=Pipe()
        killjackprocess.standardOutput = pipe
        
        killjackprocess.launch()
        
        Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { timer in
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        if let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)  {
            if output == "" {
                self.errorMessagesbox.stringValue =  "🔔 The Jack Server was already stopped !"
                UserDefaults.standard.set(false , forKey: "jack_server_launched_by_JackCoPilot")
                UserDefaults.standard.set(false , forKey: "jack_server_is_active")


            }
            else {
                UserDefaults.standard.set(false , forKey: "jack_server_launched_by_JackCoPilot")
                UserDefaults.standard.set(false , forKey: "jack_server_is_active")
            }
        }
        }
        
        
        killjackprocess.waitUntilExit()
        
        
        

        

    }
    
    
    
    
    fileprivate func checkJackRunning() -> Int {
        
        var returnvalue: Int
        returnvalue = 2
        
        let check_if_jack_is_running = ["-c", "pgrep jackdmp 2>/dev/null"]
        
        let checkjackprocess = Process()
        
        checkjackprocess.launchPath = "/bin/bash"
            
        let checkjack_arguments:[String] = check_if_jack_is_running
            
        checkjackprocess.arguments = checkjack_arguments
        
        let pipe=Pipe()
        checkjackprocess.standardOutput = pipe
        
        checkjackprocess.launch()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()

        if let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)  {
            if output != "" {
                returnvalue = 1
               if UserDefaults.standard.bool(forKey: "jack_server_is_active") != true {
                    UserDefaults.standard.set(true , forKey: "jack_server_is_active")
               }

                
            }
            else {
                returnvalue = 0
                if UserDefaults.standard.bool(forKey: "jack_server_is_active") != false {
                    UserDefaults.standard.set(false , forKey: "jack_server_is_active")
                    UserDefaults.standard.set(false , forKey: "jack_server_launched_by_JackCoPilot")
                                    }

            }
        }
        
        
       return returnvalue
        
    }
    
    
    fileprivate func inferJackParameters()  {
        
        
        let inferJackcommand = ["-c", "ps -eo args | grep jackdmp | grep -v grep"]
        
        let inferJackprocess = Process()
        
        inferJackprocess.launchPath = "/bin/bash"
            
        let inferJack_arguments:[String] = inferJackcommand
            
        inferJackprocess.arguments = inferJack_arguments
        
        let pipe=Pipe()
        inferJackprocess.standardOutput = pipe
        
        inferJackprocess.launch()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()

        if let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)  {
            
        if UserDefaults.standard.bool(forKey: "jack_server_launched_by_JackCoPilot") != true {
                UserDefaults.standard.set(output , forKey: "last_jack_command_from_external")
        }
            
        }
        
    }
    
    
    
    fileprivate func showJackNotification(launchedby_oldStateValue: Bool , isactive_oldStateValue: Bool) -> (launchedby_newStateValue: Bool , isactive_newStateValue: Bool)  {
        
        var launchedby_newStateValue: Bool = false
        var isactive_newStateValue: Bool = false
        
        if UserDefaults.standard.bool(forKey: "jack_server_launched_by_JackCoPilot") != launchedby_oldStateValue {

            launchedby_newStateValue = UserDefaults.standard.bool(forKey: "jack_server_launched_by_JackCoPilot")
       }
        else {
            launchedby_newStateValue = launchedby_oldStateValue
            
        }
        
        if UserDefaults.standard.bool(forKey: "jack_server_is_active") != isactive_oldStateValue {
            isactive_newStateValue = UserDefaults.standard.bool(forKey: "jack_server_is_active")
            if isactive_newStateValue == true{
                self.appDelegate.JackStartNotification()
                if UserDefaults.standard.bool(forKey: "jack_server_launched_by_JackCoPilot") != true {
                    inferJackParameters()
                }
                
                
            }
            else {
                self.appDelegate.JackStopNotification()
            }

       }
        
        else {
            isactive_newStateValue = isactive_oldStateValue
        }
        return (launchedby_newStateValue,isactive_newStateValue)
    }
    

    fileprivate func checkifJackExists() {
        let check_if_jack_exists = ["-c", "[ ! -f /usr/local/bin/jackdmp ] && echo \"does not exist\""]
        
        let checkjackprocess = Process()
        
        checkjackprocess.launchPath = "/bin/bash"
            
        let checkjack_arguments:[String] = check_if_jack_exists
            
        checkjackprocess.arguments = checkjack_arguments
        
        let pipe=Pipe()
        checkjackprocess.standardOutput = pipe
        
        checkjackprocess.launch()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()

        if let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)  {
            if String(output) != "" {
            errorMessagesbox.stringValue =  "⚠️ ⚠️ The Jack executable was not found at /usr/local/bin "
                jackStartButton.isEnabled = false
                jackStartButton.alphaValue = 0.5
                deviceListPopUpButton.isEnabled = false
                outputdeviceListPopUpButton.isEnabled = false
                NominalSampleRatesPopupButton.isEnabled = false
                BufferPopupButton.isEnabled = false
                hogModeCheckbox.isEnabled = false
                clockDriftCheckBox.isEnabled = false
                midiCheckbox.isEnabled = false
                NominalSampleRatesPopupButton.isEnabled = false

            }
        }
        
        
        
        
    }
    

fileprivate func populateDeviceList() {
    deviceListPopUpButton.removeAllItems()

    for device in AudioDevice.allInputDevices() {
        deviceListPopUpButton.addItem(withTitle: "In : " +  device.name + " (" + String(device.channels(direction: .recording)) + " ch.)")
        deviceListPopUpButton.lastItem?.tag = Int(device.id)
    }
    deviceListPopUpButton.addItem(withTitle: "In : No inputs")
    deviceListPopUpButton.lastItem?.tag = -1
    deviceListPopUpButton.selectItem(at : 0)

    


    if let representedAudioDevice = representedObject as? AudioDevice {
        self.deviceListPopUpButton.selectItem(withTag: Int(representedAudioDevice.id))
    }
    
    //populateNominalSampleRatesPopUpButton()

    

}
    
    fileprivate func populateOutputDeviceList() {

        outputdeviceListPopUpButton.removeAllItems()

        for device in AudioDevice.allOutputDevices() {
            outputdeviceListPopUpButton.addItem(withTitle: "Out : " + device.name + " (" + String(device.channels(direction: .playback)) + " ch.)")
            outputdeviceListPopUpButton.lastItem?.tag = Int(device.id)
        }
        
        outputdeviceListPopUpButton.addItem(withTitle: "Out : No Outputs")
        outputdeviceListPopUpButton.lastItem?.tag = -2
        

        if let representedAudioDevice = representedObject as? AudioDevice {
            self.outputdeviceListPopUpButton.selectItem(withTag: Int(representedAudioDevice.id))
        }
        
        //populateNominalSampleRatesPopUpButton()
        

    }
    
    
    
    fileprivate func populateBufferSizes() {

        BufferPopupButton.removeAllItems()
        
        let possible_buffers = [16,32,64,128,256,512,1024,2048,4096,8192]

        for buffer in possible_buffers {
            BufferPopupButton.addItem(withTitle: String(buffer))
        }
        
        //self.BufferPopupButton.selectItem(withTitle: String(512))

    }
    
    
    fileprivate func populateNominalSampleRatesPopUpButton() {

        NominalSampleRatesPopupButton.removeAllItems()

        if ( (deviceListPopUpButton.selectedItem!.tag > 0) && (outputdeviceListPopUpButton.selectedItem!.tag > 0)) {

        let input_device_id = AudioObjectID( deviceListPopUpButton.selectedItem!.tag )
        let input_device = AudioDevice.lookup(by: input_device_id)
        let output_device_id = AudioObjectID( outputdeviceListPopUpButton.selectedItem!.tag )
        let output_device = AudioDevice.lookup(by: output_device_id)
        
        
        
        
            if let inputsampleRates = input_device?.nominalSampleRates(), !inputsampleRates.isEmpty {
                if let outputsampleRates = output_device?.nominalSampleRates(), !outputsampleRates.isEmpty {
            NominalSampleRatesPopupButton.isEnabled = true
                let sampleRates = inputsampleRates.filter(outputsampleRates.contains)
                    
            for sampleRate in sampleRates {
                NominalSampleRatesPopupButton.addItem(withTitle: format(sampleRate: sampleRate))
                NominalSampleRatesPopupButton.lastItem?.representedObject = sampleRate
            }

                    if let nominalSampleRate = input_device?.nominalSampleRate() {
                NominalSampleRatesPopupButton.selectItem(withRepresentedObject: nominalSampleRate)
            }
        } else {
            NominalSampleRatesPopupButton.addItem(withTitle: unsupportedValue)
            NominalSampleRatesPopupButton.isEnabled = false
        }
                
                if let nominalSampleRate = output_device?.nominalSampleRate() {
            NominalSampleRatesPopupButton.selectItem(withRepresentedObject: nominalSampleRate)
        }
    } else {
        NominalSampleRatesPopupButton.addItem(withTitle: unsupportedValue)
        NominalSampleRatesPopupButton.isEnabled = false
    }
    }
        
        
        
        else if ( (deviceListPopUpButton.selectedItem!.tag < 0) && (outputdeviceListPopUpButton.selectedItem!.tag > 0)) {

        let output_device_id = AudioObjectID( outputdeviceListPopUpButton.selectedItem!.tag )
        let output_device = AudioDevice.lookup(by: output_device_id)
        
        
        
        
 
                if let outputsampleRates = output_device?.nominalSampleRates(), !outputsampleRates.isEmpty {
            NominalSampleRatesPopupButton.isEnabled = true
                let sampleRates = outputsampleRates
                    
            for sampleRate in sampleRates {
                NominalSampleRatesPopupButton.addItem(withTitle: format(sampleRate: sampleRate))
                NominalSampleRatesPopupButton.lastItem?.representedObject = sampleRate
            }
} else {
            NominalSampleRatesPopupButton.addItem(withTitle: unsupportedValue)
            NominalSampleRatesPopupButton.isEnabled = false
        }
                
                if let nominalSampleRate = output_device?.nominalSampleRate() {
            NominalSampleRatesPopupButton.selectItem(withRepresentedObject: nominalSampleRate)
        }
     else {
        NominalSampleRatesPopupButton.addItem(withTitle: unsupportedValue)
        NominalSampleRatesPopupButton.isEnabled = false
    }
    }
        
        
        else if ( (deviceListPopUpButton.selectedItem!.tag > 0) && (outputdeviceListPopUpButton.selectedItem!.tag < 0)) {

        let input_device_id = AudioObjectID( deviceListPopUpButton.selectedItem!.tag )
        let input_device = AudioDevice.lookup(by: input_device_id)
        
        
        
        
 
                if let inputsampleRates = input_device?.nominalSampleRates(), !inputsampleRates.isEmpty {
            NominalSampleRatesPopupButton.isEnabled = true
                let sampleRates = inputsampleRates
                    
            for sampleRate in sampleRates {
                NominalSampleRatesPopupButton.addItem(withTitle: format(sampleRate: sampleRate))
                NominalSampleRatesPopupButton.lastItem?.representedObject = sampleRate
            }
} else {
            NominalSampleRatesPopupButton.addItem(withTitle: unsupportedValue)
            NominalSampleRatesPopupButton.isEnabled = false
        }
                
                if let nominalSampleRate = input_device?.nominalSampleRate() {
            NominalSampleRatesPopupButton.selectItem(withRepresentedObject: nominalSampleRate)
        }
     else {
        NominalSampleRatesPopupButton.addItem(withTitle: unsupportedValue)
        NominalSampleRatesPopupButton.isEnabled = false
    }
    }
        
        
        
        else if ( (deviceListPopUpButton.selectedItem!.tag < 0) && (outputdeviceListPopUpButton.selectedItem!.tag < 0)) {

            NominalSampleRatesPopupButton.addItem(withTitle: unsupportedValue)
            NominalSampleRatesPopupButton.isEnabled = false
            
        }
            
        

    }
    
    
    
    fileprivate func getAgregateDeviceList() {

        listofAgregateDevices.removeAll()
        
        listofAgregateInputDevices.removeAll()
        
        listofAgregateOutputDevices.removeAll()

        
        for device in AudioDevice.allDevices() {
            if device.transportType?.rawValue == "aggregate" {
                listofAgregateDevices.append(Int(device.id))
            }
        }
        
        for device in AudioDevice.allInputDevices() {
            if device.transportType?.rawValue == "aggregate" {
                listofAgregateInputDevices.append(Int(device.id))
            }
        }
        
        for device in AudioDevice.allOutputDevices() {
            if device.transportType?.rawValue == "aggregate" {
                listofAgregateOutputDevices.append(Int(device.id))
            }
        }
      


    }
        

    
    fileprivate func populateDeviceInformation(device: AudioDevice) {

       
        if ( (deviceListPopUpButton.selectedItem!.tag > 0) && (outputdeviceListPopUpButton.selectedItem!.tag > 0)) {

        let input_device_id = AudioObjectID( deviceListPopUpButton.selectedItem!.tag )
        let input_device = AudioDevice.lookup(by: input_device_id)
        let output_device_id = AudioObjectID( outputdeviceListPopUpButton.selectedItem!.tag )
        let output_device = AudioDevice.lookup(by: output_device_id)

            
            if let actualSampleRate = input_device?.actualSampleRate() {
                currentInputSampleRate.stringValue = "Current Sample Rate  : " + format(sampleRate: actualSampleRate)
            } else {
                currentInputSampleRate.stringValue = "Current Sample Rate  : " + unknownValue
            }
            
            if let actualSampleRate = output_device?.actualSampleRate() {
            currentOutputSampleRate.stringValue = "Current Sample Rate  : " + format(sampleRate: actualSampleRate)
            } else {
                currentOutputSampleRate.stringValue = "Current Sample Rate  : " + unknownValue
            }
            
            if let buffer_value = BufferPopupButton.selectedItem?.title {
                if let samplerate_value = NominalSampleRatesPopupButton.selectedItem?.representedObject as? Float64 {
                    theoreticalLatency.stringValue = String(format : "%.1f ms", 1000/samplerate_value*Double(buffer_value)!)
                }
            }
            
        }
        
        

        
            else if ((deviceListPopUpButton.selectedItem!.tag > 0) && (outputdeviceListPopUpButton.selectedItem!.tag < 0)) {
                
                let input_device_id = AudioObjectID( deviceListPopUpButton.selectedItem!.tag )
                let input_device = AudioDevice.lookup(by: input_device_id)
                                
                if let actualSampleRate = input_device?.actualSampleRate() {
                currentInputSampleRate.stringValue = "Current Sample Rate  : " + format(sampleRate: actualSampleRate)
                } else {
                    currentInputSampleRate.stringValue = "Current Sample Rate  : " + unknownValue
                }
                
                currentOutputSampleRate.stringValue = "Current Sample Rate  : " + unknownValue
            
            }
        
            else if ((deviceListPopUpButton.selectedItem!.tag < 0) && (outputdeviceListPopUpButton.selectedItem!.tag > 0)) {
                
                let output_device_id = AudioObjectID( outputdeviceListPopUpButton.selectedItem!.tag )
                let output_device = AudioDevice.lookup(by: output_device_id)


                if let actualSampleRate = output_device?.actualSampleRate() {
                currentOutputSampleRate.stringValue = "Current Sample Rate  : " + format(sampleRate: actualSampleRate)
                } else {
                    currentInputSampleRate.stringValue = "Current Sample Rate  : " + unknownValue
                }
                
                currentInputSampleRate.stringValue = "Current Sample Rate  : " + unknownValue
                
                }
            
            
   
            else if ((deviceListPopUpButton.selectedItem!.tag < 0) && (outputdeviceListPopUpButton.selectedItem!.tag < 0)) {
                

                errorMessagesbox.stringValue =  "⚠️ Please at least select an Input or Output device !"
                
                
                currentInputSampleRate.stringValue = "Current Sample Rate  : " + unknownValue
                currentOutputSampleRate.stringValue = "Current Sample Rate  : " + unknownValue
                
                }
        
        //populateDeviceList()
        //populateOutputDeviceList()
        populateNominalSampleRatesPopUpButton()
                
        }
    
    
    fileprivate enum /* namespace */ Shell {
        static func run(command: String, arguments: [String] = []) throws -> (stdout: String?, stderr: String?) {
            let process = Process()
            if #available(OSX 10.13, *) {
                process.executableURL = URL(fileURLWithPath: command)
            } else {
                // Fallback on earlier versions
            }
            process.arguments = arguments

            let pipes = (stdout: Pipe(), stderr: Pipe())

            process.standardOutput = pipes.stdout
            process.standardError = pipes.stderr
            
            if #available(OSX 10.13, *) {
                try process.run()
            } else {
                // Fallback on earlier versions
            }

            /* nested */
            func siphon(_ pipe: Pipe) -> String? {
                let data = pipe.fileHandleForReading.readDataToEndOfFile()
                guard data.count > 0 else { return nil }
                return String(decoding: data, as: UTF8.self)
            }

            return (siphon(pipes.stdout), siphon(pipes.stderr))
        }
    }

    
}





extension ViewController: EventSubscriber {
    func eventReceiver(_ event: Event) {
        switch event {
        case let event as AudioDeviceEvent:
            switch event {
            case let .nominalSampleRateDidChange(audioDevice):
                populateDeviceInformation(device: audioDevice)
                if representedObject as? AudioDevice == audioDevice {
                    populateDeviceInformation(device: audioDevice)
                }
            case let .nameDidChange(audioDevice):
                if representedObject as? AudioDevice == audioDevice {
                        populateDeviceInformation(device: audioDevice)
                }

                if let item = deviceListPopUpButton.item(withTag: Int(audioDevice.id)) {
                    item.title = audioDevice.name
                }
            case let .listDidChange(audioDevice):
                if representedObject as? AudioDevice == audioDevice {
                    populateDeviceInformation(device: audioDevice)
                }
            default:
                break
            }
        case let event as AudioHardwareEvent:
            switch event {
            case .deviceListChanged:
                errorMessagesbox.stringValue =  "🔔 The list of Audio devices has just been updated."
                self.populateDeviceList()
                self.populateOutputDeviceList()
                self.showExistingPrefs()
                self.saveJackcommand()
            case let .defaultInputDeviceChanged(audioDevice):
                errorMessagesbox.stringValue =  "🔔 The default (system) input device changed to \(audioDevice)"
            case let .defaultOutputDeviceChanged(audioDevice):
                errorMessagesbox.stringValue =  "🔔 The default (system) output device changed to \(audioDevice)"
            case let .defaultSystemOutputDeviceChanged(audioDevice):
                errorMessagesbox.stringValue =  "🔔 The default (system) output device changed to \(audioDevice)"
            }
        default:
            break
        }
    }
}
