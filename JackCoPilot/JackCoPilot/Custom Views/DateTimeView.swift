//
//  DateTimeView.swift
//  WorldTime
//
//  Created by Gabriel Theodoropoulos.
//  Copyright © 2020 AppCoda. All rights reserved.
//

import Cocoa
import AMCoreAudio

class DateTimeView: NSView, LoadableView {

    // MARK: - IBOutlet Properties
    
    @IBOutlet weak var jackStatusmonitor_Label: NSTextField!
    
    @IBOutlet weak var parameterstitle_Label: NSTextField!

        
    @IBOutlet weak var inputChannel_Label: NSTextField!

    @IBOutlet weak var outpuChannel_Label: NSTextField!

    @IBOutlet weak var samplerate_buffersize_Label: NSTextField!
    
    @IBOutlet weak var hog_drift_Label: NSTextField!
    
    @IBOutlet weak var midi_Label: NSTextField!

    
    
    // MARK: - Properties
    
    var timer: Timer?
    
    var preferredTimezoneID: String?
    
    
    // MARK: - Init
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        _ = load(fromNIBNamed: "DateTimeView")
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    
    // MARK: - Custom Fileprivate Methods
    
    fileprivate func format(sampleRate: Float64) -> String {
        return String(format: "%.1f kHz", sampleRate / 1000)
    }
    
    
   
            
    fileprivate func getJackCPU_usage()  {
        
        
        let JackCPUcommand = ["-c", "ps -eo pcpu,args | grep jackdmp | grep -v grep"]
        
        let JackCPUprocess = Process()
        
        JackCPUprocess.launchPath = "/bin/bash"
            
        let JackCPU_arguments:[String] = JackCPUcommand
            
        JackCPUprocess.arguments = JackCPU_arguments
        
        let pipe=Pipe()
        JackCPUprocess.standardOutput = pipe
        
        JackCPUprocess.launch()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()

        if let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)  {
                    
           let outputString: String = output as String
            if let CPUindex = outputString.firstIndex(of: "/") {
                    let CPUpercent = outputString.prefix(upTo: CPUindex)
                jackStatusmonitor_Label.stringValue = "Jack is running   -" + CPUpercent + "% CPU"
                }
                
        }
    }
        
    
    
    @objc fileprivate func showDateAndTimeInfo() {
                
        struct chosenPreferences {

          // 1
          var chosenInputDevice: String {
            get {
              // 2
                let savedInputDevice = UserDefaults.standard.string(forKey: "chosenInputDevice")
              if savedInputDevice != nil {
                return savedInputDevice!
              }
              // 3
              return ""
            }
            set {
              // 4
              UserDefaults.standard.set(newValue, forKey: "chosenInputDevice")
            }
          }
            
            // 1
            var chosenOutputDevice: String {
              get {
                // 2
                  let savedOutputDevice = UserDefaults.standard.string(forKey: "chosenOutputDevice")
                if savedOutputDevice != nil {
                  return savedOutputDevice!
                }
                // 3
                return ""
              }
              set {
                // 4
                UserDefaults.standard.set(newValue, forKey: "chosenOutputDevice")
              }
            }
            
            var chosenAggregateDevice: String {
              get {
                // 2
                  let savedAggregateDevice = UserDefaults.standard.string(forKey: "chosenAggregateDevice")
                if savedAggregateDevice != nil {
                  return savedAggregateDevice!
                }
                // 3
                return ""
              }
              set {
                // 4
                UserDefaults.standard.set(newValue, forKey: "chosenAggregateDevice")
              }
            }
            
            var chosenAggregateDeviceTag: Int {
              get {
                // 2
                  let savedAggregateDeviceTag = UserDefaults.standard.integer(forKey: "chosenAggregateDeviceTag")
                if savedAggregateDeviceTag > 0 {
                    return savedAggregateDeviceTag
                }
                // 3
                return -1
              }
              set {
                // 4
                UserDefaults.standard.set(newValue, forKey: "chosenAggregateDeviceTag")
              }
            }
            
            
            // 1
            var chosenSampleRate: String {
              get {
                // 2
                  let savedSampleRate = UserDefaults.standard.string(forKey: "chosenSampleRate")
                if savedSampleRate != nil {
                  return savedSampleRate!
                }
                // 3
                return ""
              }
              set {
                // 4
                UserDefaults.standard.set(newValue, forKey: "chosenSampleRate")
              }
            }
            
            var chosenBufferSize: String {
              get {
                // 2
                  let savedBufferSize = UserDefaults.standard.string(forKey: "chosenBufferSize")
                if savedBufferSize != nil {
                  return savedBufferSize ?? "256"
                }
                // 3
                return "256"
              }
              set {
                // 4
                UserDefaults.standard.set(newValue, forKey: "chosenBufferSize")
              }
            }
            
            
            var chosenHogMode: String {
              get {
                // 2
                  let savedHogMode = UserDefaults.standard.string(forKey: "chosenHogMode")
                if savedHogMode != nil {
                  return savedHogMode!
                }
                // 3
                return ""
              }
              set {
                // 4
                UserDefaults.standard.set(newValue, forKey: "chosenHogMode")
              }
            }
            
            var chosenClockDrift: String {
              get {
                // 2
                  let savedClockDrift = UserDefaults.standard.string(forKey: "chosenClockDrift")
                if savedClockDrift != nil {
                  return savedClockDrift!
                }
                // 3
                return ""
              }
              set {
                // 4
                UserDefaults.standard.set(newValue, forKey: "chosenClockDrift")
              }
            }
            
            var chosenMidiUsage: String {
              get {
                // 2
                  let savedMidiUsage = UserDefaults.standard.string(forKey: "chosenMidiUsage")
                if savedMidiUsage != nil {
                  return savedMidiUsage!
                }
                // 3
                return ""
              }
              set {
                // 4
                UserDefaults.standard.set(newValue, forKey: "chosenMidiUsage")
              }
            }
            
        }
        
        let chosenprefs = chosenPreferences()

        
        if UserDefaults.standard.bool(forKey: "jack_server_is_active") == true {
            
            getJackCPU_usage()
            
            if UserDefaults.standard.bool(forKey: "jack_server_launched_by_JackCoPilot") == true {
                
            parameterstitle_Label.stringValue = "Current Jack parameters :"
                
                inputChannel_Label.stringValue = chosenprefs.chosenInputDevice
                
                outpuChannel_Label.stringValue = chosenprefs.chosenOutputDevice
                
                samplerate_buffersize_Label.stringValue = "Samplerate : " + chosenprefs.chosenSampleRate + " - Buffersize : " + chosenprefs.chosenBufferSize
                
                hog_drift_Label.stringValue = "Hog mode " + chosenprefs.chosenHogMode + " - Clock drift correction " + chosenprefs.chosenClockDrift
                
                midi_Label.stringValue = "Midi usage " + chosenprefs.chosenMidiUsage
            }
            else {
                parameterstitle_Label.stringValue = "Current Jack parameters (inferred) :"
                
                if let parsed = UserDefaults.standard.string(forKey: "last_jack_command_from_external")?.replacingOccurrences(of: "-d coreaudio", with: "").replacingOccurrences(of: "\n", with: "").replacingOccurrences(of: "\n", with: "") {
                    
                    if parsed.contains("coremidi"){
                        midi_Label.stringValue = "Midi usage " + "On"
                    }
                    else {
                        midi_Label.stringValue = "Midi usage " + "Off"
                    }
                    
                    if parsed.contains("-s "){
                        if parsed.contains("-H "){
                            hog_drift_Label.stringValue = "Hog mode On" + " - Clock drift correction On"
                        }
                        else {
                            hog_drift_Label.stringValue = "Hog mode Off" + " - Clock drift correction On"
                        }

                    }
                    else {
                        if parsed.contains("-H "){
                            hog_drift_Label.stringValue = "Hog mode On" + " - Clock drift correction Off"
                        }
                        else {
                            hog_drift_Label.stringValue = "Hog mode Off" + " - Clock drift correction Off"
                        }
                    }
                    
                    
                    let samplerate_range = parsed.range(of: "-r ")
                    let samplerate  = String(parsed[samplerate_range!.upperBound...]).components(separatedBy: " ").first! + " Hz"
                    
                    
                    let buffersize_range = parsed.range(of: "-p ")
                    let buffersize  = String(parsed[buffersize_range!.upperBound...]).components(separatedBy: " ").first!
                    
                    
                    samplerate_buffersize_Label.stringValue = "Samplerate : " + samplerate + " - Buffersize : " + buffersize
                    
                    
                    if parsed.contains("-d "){
                        
                        let aggregate_range = parsed.range(of: "-d ")
                        let aggregatedevicename  = String(parsed[aggregate_range!.upperBound...]).components(separatedBy: " ").first!
                        
                        
                        inputChannel_Label.stringValue = aggregatedevicename
                        
                        outpuChannel_Label.stringValue = aggregatedevicename
                        
                        
                    }
                    else {
                        
                        if parsed.contains("-C "){
                            
                            let inputdevice_range = parsed.range(of: "-C ")
                            let inputdevicename  = String(parsed[inputdevice_range!.upperBound...]).components(separatedBy: " ").first!
                            
                            inputChannel_Label.stringValue = "In : Did not infer"

                            for device in AudioDevice.allInputDevices() {
                                if device.uid == inputdevicename {
                                    inputChannel_Label.stringValue = "In : " +  device.name + " (" + String(device.channels(direction: .recording)) + " ch.)"
                                }

                        }
                        }
        
                        else {
                            inputChannel_Label.stringValue = "In : No inputs"
                        }
                        
                        
                        if parsed.contains("-P "){
                            
                            let outputdevice_range = parsed.range(of: "-P ")
                            let outputdevicename  = String(parsed[outputdevice_range!.upperBound...]).components(separatedBy: " ").first!
                            
                            outpuChannel_Label.stringValue = "Out : Did not infer"

                        
                            for device in AudioDevice.allOutputDevices() {
                                if device.uid == outputdevicename {

                                    outpuChannel_Label.stringValue = "Out : " +  device.name + " (" + String(device.channels(direction: .playback)) + " ch.)"
                                }
                        }
                        }
                        
                        else {
                            outpuChannel_Label.stringValue = "Out : No outputs"
                        }
                        
                    }
                }

            }
        }
        else {
            jackStatusmonitor_Label.stringValue = "Jack is stopped."
            parameterstitle_Label.stringValue = "Current parameters selected :"
            
            inputChannel_Label.stringValue = chosenprefs.chosenInputDevice
            
            outpuChannel_Label.stringValue = chosenprefs.chosenOutputDevice
            
            samplerate_buffersize_Label.stringValue = "Samplerate : " + chosenprefs.chosenSampleRate + " - Buffersize : " + chosenprefs.chosenBufferSize
            
            hog_drift_Label.stringValue = "Hog mode " + chosenprefs.chosenHogMode + " - Clock drift correction " + chosenprefs.chosenClockDrift
            
            midi_Label.stringValue = "Midi usage " + chosenprefs.chosenMidiUsage

        }
        

        
    }
    
    
    // MARK: - Internal Methods
    
    func startTimer() {
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(showDateAndTimeInfo), userInfo: nil, repeats: true)
        timer?.fire()
        
        RunLoop.current.add(timer!, forMode: .common)
    }
    
    
    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
}
