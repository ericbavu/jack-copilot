## Jack Copilot

[![Platform](https://img.shields.io/badge/Platforms-macOS%20-4E4E4E.svg?colorA=28a745)]
[![Swift support](https://img.shields.io/badge/Swift-4.0%20%7C%204.2%20%7C%205.0%20%7C%205.1%20-lightgrey.svg?colorA=28a745&colorB=4E4E4E)]

[![License](https://img.shields.io/github/license/mashape/apistatus.svg)]

`Jack Copilot` is a Swift application written by Eric Bavu, Cnam Paris.

`Jack Copilot`  consists in a  re-write from scratch of JackPilot in Swift language for use with modern 64 bits macOS systems (targeted for Mojave, Catalina, and Big Sur versions).

The GUI heavily inspired on the original JackPilot 1.7.3 application created at Grame by Stéphane Letz and Johnny Petrantoni. 

All macOS Coreaudio specific functions are achieved using the Swift `AMCoreAudio` framework written by Ruben Nine, which is a framework that aims to make [Core Audio](https://developer.apple.com/library/mac/documentation/MusicAudio/Conceptual/CoreAudioOverview/) use less tedious in macOS.

On top of that, I also compiled `Patchage` 1.0.2 from David Robillard for use on Catalina and higher versions of macOS, so that Jack Copilot can be seconded by an excellent graphical wiring app for use with Jack on macOS.

### Installation for Jack Copilot 

The easiest way to install is to **download the [dmg installer](https://gitlab.com/ericbavu/jack-copilot/-/raw/master/JackCoPilot_1.2.1.dmg)** (which also includes a compiled version of Patchage 1.0.2 from David Robillard, compatible for macOS versions from Catalina). 

Note : If you intend to install Patchage for Mojave or lower versions, please download 1.0.1 version at [David Robillard website](http://drobilla.net/files/Patchage.zip) 

Jack Copilot on its own does not require Patchage (and vice versa), but these two tools are complimentary.

### Sources 

All Jack-Copilot related sources are available on this repository, feel free to open the xcode project to compile it on your own computer. Patchage sources are available at [Drobila's gitlab](https://gitlab.com/drobilla/patchage), and AMCore Audio sources are available at [rnine's github](https://github.com/rnine/AMCoreAudio). 

### Requirements for Jack Copilot 

- macOS > 10.10 (only tested on Mojave and Catalina)
- Jack > 1.9.11 (or JackOSX 0.92b3)
- Xcode > 10.2 for compilation of the xcode project (should not be mandatory, since I also put a .dmg installer in this Gitlab archive) 
- Swift 4.0 / 4.2 / 5.0 / 5.1



JackCopilot preferences tab : 

![img](https://gitlab.com/ericbavu/jack-copilot/-/raw/master/Screenshots/Capture%20d%E2%80%99%C3%A9cran%202021-01-06%20%C3%A0%2010.59.57.png)





JackCopilot easy access to useful tools tab : 

![Capture d’écran 2021-01-06 à 11.00.26.png](https://gitlab.com/ericbavu/jack-copilot/-/raw/master/Screenshots/Capture%20d%E2%80%99%C3%A9cran%202021-01-06%20%C3%A0%2011.00.26.png)

Patchage 1.0.2 in action on Catalina :  

![Capture d’écran 2021-01-06 à 11.22.30.png](https://gitlab.com/ericbavu/jack-copilot/-/raw/master/Screenshots/Capture%20d%E2%80%99%C3%A9cran%202021-01-06%20%C3%A0%2011.22.30.png)





### Documentation

- [AmCore Audio API Documentation](https://rnine.github.io/AMCoreAudio)
- JackCopilot is not yet documented, contributions welcome ! Although, since I developped Jack Copilot using only the JackPilot documentation and JackPilot screeenshots, the documentation should help (although JackCopilot does not include any JackRouter functionnality)

### Further Development & Patches

Do you want to contribute to the project? Please fork, patch, and then submit a pull request!

### License

`JackCopilot` was written by Eric Bavu in 2020-2021  and is licensed under the [MIT](https://opensource.org/licenses/MIT) license. See [LICENSE.md](LICENSE.md).

`AMCoreAudio` was written by Ruben Nine ([@sonicbee9](https://twitter.com/sonicbee9)) in 2013-2014 (open-sourced in March 2014) and is licensed under the [MIT](https://opensource.org/licenses/MIT) license. See [LICENSE.md](https://github.com/rnine/AMCoreAudio/blob/develop/README.md).

`Patchage` was written by David Robillard in 2020-2021  and is licensed under the [GPL v3](https://gitlab.com/drobilla/patchage/-/blob/master/COPYING) license. 

