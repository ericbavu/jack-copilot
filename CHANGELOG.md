## Changelog

| Version        | Description| Date     |
| -------------:|:------------- |:--------:|
|   1.3.12 | Implemented CPU monitor for the Jack process in the custom info view in the menubar extra.  | 13 Jan 2021  |
|   1.3.11 | Found a way (and implemented) to infer the jack parameters at used when Jack was started by another client by parsing ps -eo -args . This is now also shown on the custom info view in the menubar (although if the buffersize has changed after launch, this will only show the "original" buffersize, but I honestly don't care ...) | 13 Jan 2021  |
|   1.3.10 | Added information about the number of channels available for capture and playback.  | 13 Jan 2021  |
|   1.3.9 | Bugfix : check if item exists in popupbuttonslists when populating. Prevents a crash when a device was present and selected the last time JackCoPilot was closed and not present anymore at restart. | 13 Jan 2021  |
|   1.3.8 | Added a custom view to the menubar to see the current status and parameters of the Jack server | 12 Jan 2021  |
|   1.3.7 | Use a private API to show a button on notification banner when Jack is stopped to immediatly launch the main window (useful when Jack stops unexpectedly) | 12 Jan 2021  |
|   1.3.6 | Use of saveJackcommand() to log in plist / modify the jack commandline to execute, each time the user interacts with any GUI element in the main window | 12 Jan 2021  |
|   1.3.5 | Bugfix : Better handling of macOS aggregate devices (automatically disable hogmode when a macOS aggregate device is selected). | 11 Jan 2021  |
|   1.3.4 | Better handling of (empty) preferences plist at the first startup  | 11 Jan 2021  |
|   1.3.3 | Added start notification center information about start / stop of the Jack server, even if it has been initiated by another software than Jack Copilot | 11 Jan 2021  |
|   1.3.2 | Extensive use of UserDefaults.standard dictionnary to log selected values and recall selected parameters at startup (saved in a standard .plist)  | 10 Jan 2021  |
|   1.3.1 | Added start / stop menubar menus that work even if window is completely hidden | 10 Jan 2021  |
|   1.3.0 | Added menubar functionnality - act as a background process without Dock icon if main window is closed. | 9 Jan 2021  |
|   1.2.2 | Improved monitoring of Jack current status | 8 Jan 2021  |
|   1.2.1 | Bugfix : jackdmp  exec typo  / Added check if jackdmp has successfully started | 7 Jan 2021  |
|     1.2 | Added checks if jack binary is present in /usr/local/bin and if jack is already running at JackCoPilot startup | 7 Jan 2021  |
|     1.1 | Added Tools tab for easy access to configuration and Patchage | 6 Jan 2021  |
|     1.0 | Initial Release                                              | 19 Dec 2020 |

